

----------------

# Repository layout

* [dict_reducer](dict_reducer/) contains sources to build the program that reduces the size of dictionary.
* [find_buggy_passwords](find_buggy_passwords/) contains sources to build a program finding passwords needing more than 20 iterations to be derived into a group element on P256
* [simulation](simulation/) is a core directory. It contains scripts to setup a test environnement, with a fake AP running hostapd and a fake client running iwd. More information are available inside the directory.
* [spy](spy/) contains the sources to build the spy process.
* [trace_interpretation](trace_interpretation/) contains scripts related to parsing and automatic interpretaion of the collected samples.

# Attack on iwd 

## Core idea

This cache attack targets the Dragonfly exchange implemented in iwd. It exploits password / control flow dependency in the implementation. This dependency occurs in the hunting and pecking procedure used to convert the password into an elliptic curve point. 
Due to the try-and-increment nature of this procedure, attackers can guess at which iteration the password has successfuly been converted into a point, thus reducing the number of possible passwords.

Since the conversion depends not only on the password, but also on both parties' MAC addresses, acquiering several measures on the same password and different addresses allow attackers to tremendously reduce the size of the dictionary.

## Threat model

To exploit it, the attackers need to be able to monitor the CPU cache, using a classical Flus+Reload attack for instance. To do so, we assume the attackers are able to deploy a spy process, with no specific priviledges other than being able to read the iwd binary (which is a default permissions).

This spy process is assumed to run in background in order to record the CPU cache access to some specific functions.


## To Start
* run "./simulation/setup.sh -i" to build the test environment (this might take a while)
* run "sudo ./simulation/poc.sh -n 14 -d -p res_traces/<an existing directory>" in order to interpret the generated traces.
* run "sudo ./simulation/poc.sh -n 14 -s res_traces/<a new directory>" in order to generate new traces.


## More details can be found in README inside the simulation directory.


