
#include "main.h"

static bool sae_pwd_seed(const uint8_t *addr1, const uint8_t *addr2,
                        uint8_t *base, size_t base_len,
                        uint8_t counter, uint8_t *out)
{
    uint8_t key[12];

    if (memcmp(addr1, addr2, 6) > 0) {
        memcpy(key, addr1, 6);
        memcpy(key + 6, addr2, 6);
    } else {
        memcpy(key, addr2, 6);
        memcpy(key + 6, addr1, 6);
    }

    return hkdf_extract(L_CHECKSUM_SHA256, key, 12, 2, out, base, base_len,
                        &counter, (size_t) 1);
}

static struct l_ecc_scalar *sae_pwd_value(const struct l_ecc_curve *curve,
                                         uint8_t *pwd_seed)
{
    uint8_t pwd_value[L_ECC_SCALAR_MAX_BYTES];
    uint8_t prime[L_ECC_SCALAR_MAX_BYTES];
    ssize_t len;
    struct l_ecc_scalar *p = l_ecc_curve_get_prime(curve);

    len = l_ecc_scalar_get_data(p, prime, sizeof(prime));

    l_ecc_scalar_free(p);

    if (!kdf_sha256(pwd_seed, 32, "SAE Hunting and Pecking",
                    strlen("SAE Hunting and Pecking"), prime, len,
                    pwd_value, len))
        return NULL;

    return l_ecc_scalar_new(curve, pwd_value, sizeof(pwd_value));
}


bool do_round(uint8_t *password, const uint8_t *addr1, const uint8_t *addr2, const uint8_t round, uint8_t * seed, const struct l_ecc_curve *curve)
{
    bool found = false;
    struct l_ecc_scalar *value = NULL;
    struct l_ecc_scalar *y_sqr = l_ecc_scalar_new(curve, NULL, 0);

    /* pwd-seed = H(max(addr1, addr2) || min(addr1, addr2),
     *                base || counter)
     * pwd-value = KDF-256(pwd-seed, "SAE Hunting and Pecking", p)
     */
    sae_pwd_seed(addr1, addr2, password, strlen(password), round, seed);

    value = sae_pwd_value(curve, seed);
    if (!value)
        goto end_round;

    // Check if we got a quadratic residue
    l_ecc_scalar_sum_x(y_sqr, value);
    found = l_ecc_scalar_legendre(y_sqr) == 1;

    end_round:
    if (y_sqr) {l_ecc_scalar_free(y_sqr);}
    if (value) {l_ecc_scalar_free(value);}

    return found;
}

int get_round(uint8_t *pwd, uint8_t *macA, uint8_t *macB, struct l_ecc_curve *curve)
{
    uint8_t seed[32];
    bool ok = false;
    int counter = 1;
    while (!do_round(pwd, macA, macB, counter, seed, curve))
        counter++;

    return counter;
}