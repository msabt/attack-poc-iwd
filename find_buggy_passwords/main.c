#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include  <omp.h>
#include <time.h>

#include "main.h"

#define MAX_PWD_SIZE 34

int get_round(uint8_t *pwd, uint8_t *macA, uint8_t *macB, struct l_ecc_curve *curve);

void usage(const char *name)
{
	fprintf(stderr, "USAGE: %s DICT", name);
	fprintf(stderr, "\n\t* DICT is a dictionnary of passwords containg one entry per line\n");
}

// void parseMAC(char *str, uint8_t *mac) {
// 	uint64_t tmp;

// 	// First field is the address A
// 	tmp = strtol(str, &str, 16);
// 	for (int i = 5; i >= 0; --i)
// 	{
// 		mac[i] = tmp & 0XFF;
// 		tmp = tmp >> 8;
// 	}
// 	str++;
// }

void getMAC(uint8_t *mac) {
	for (int i = 0; i < 6; i++)
 	{
 		mac[i] = rand() & 0XFF;
 	}
}

void printHeader(uint8_t *macA, uint8_t *macB) {
	fprintf(stdout, "\n\n## ");
	for(int i = 0; i < 6; i++) {
		fprintf(stdout, "%02X", macA[i]);
	}
	fprintf(stdout, " ");
	for(int i = 0; i < 6; i++) {
		fprintf(stdout, "%02X", macB[i]);
	}
	fprintf(stdout, " ##\n");
}


int main(int argc, char const *argv[])
{
	FILE *fp = NULL;
	char *Talloc = NULL;
	char **dict = NULL;
	char *pwd = NULL;
    struct sae_sm *sm = NULL;
	uint8_t macA[6] = {0};
    uint8_t macB[6] = {0};

    int seed = time(NULL);
	srand(seed);
	fprintf(stdout, "seed: %d\n", seed);

	if (argc < 2) {
		usage(argv[0]);
		goto end;
	}

	fp = fopen(argv[1], "r");
	if (!fp) {
		fprintf(stderr, "Error while opening file %s\n", argv[1]);
		goto end;
	}

	int nbLine = 0;
	while(!feof(fp))
		if(fgetc(fp) == '\n')
			nbLine++;
	rewind(fp);

	// We read the file once and for all, storing all password in RAM
	Talloc = malloc(nbLine*MAX_PWD_SIZE);
	dict = malloc(nbLine*sizeof(char*));
	for(int i = 0 ; i < nbLine ; i++)
		dict[i] = &Talloc[MAX_PWD_SIZE*i];

	// Read a line and remove the trailing '\n'
	for(int i = 0; i < nbLine; i++) {
        fgets(dict[i], MAX_PWD_SIZE, fp);
        dict[i][strlen(dict[i]) - 1] = 0;
	}
    fclose(fp); fp = NULL;

	// Setup the structure context
	sm = l_new(struct sae_sm, 1);
    if (!sm) {
        goto end;
    }
    sm->ecc_groups = l_ecc_curve_get_supported_ike_groups();
    if (!sm->ecc_groups) {
        goto end;
    }
    sm->group = sm->ecc_groups[sm->group_retry];
    if (!sm->group) {
        goto end;
    }
    sm->curve = l_ecc_curve_get_ike_group(sm->group);
    if (!sm->curve) {
        goto end;
    }
	struct l_ecc_curve * curve = sm->curve;
	
	while (1)
	{
		getMAC(macA);
		getMAC(macB);
		printHeader(macA, macB);
		fflush(stdout);
		
		omp_set_num_threads(omp_get_max_threads());
		#pragma omp parallel for shared(dict, macA, macB, curve) schedule(static)
		for(int i = 0; i < nbLine; i++) {
			int n = get_round(dict[i], macA, macB, curve);
			if (n > 20) {
				#pragma omp critical
				fprintf(stdout, "%s %d\n", dict[i], n);
				fflush(stdout);
			}
		}
	}

	end:
	if (fp) {fclose(fp);}
	if (sm) {l_free(sm);}
	free(Talloc);
	free(dict);

	return 0;
}