#!/bin/bash

get_rockyou() {
    [ -e rockyou.txt.bz2 ] || wget https://www.scrapmaker.com/data/wordlists/dictionaries/rockyou.txt.bz2 
    [ -e rockyou.txt ] || tar xjvf rockyou.txt.bz2
    # Keep only password wich are long enough but not too long
    [ -e rockyou_resized.txt ] || awk '(length > 8) && (length < 33) {print $0}' rockyou.txt > rockyou_resized.txt
    rm rockyou.txt.bz2 rockyou.txt
}

get_crackstation() {
    [ -e crackstation-human-only.txt.gz ] || wget https://crackstation.net/files/crackstation-human-only.txt.gz 
    [ -e crackstation-human-only.txt ] || tar xzvf crackstation-human-only.txt.gz
    # Keep only password wich are long enough but not too long
    [ -e crackstation-human-only_resized.txt ] || awk '(length > 8) && (length < 33) {print $0}' crackstation-human-only.txt > crackstation-human-only_resized.txt
    rm crackstation-human-only.txt.gz crackstation-human-only.txt
}

DICT="my-dict.txt" 

# get the dictionnaries
if [ -n $DICT ]
then
    echo "Creating a dictionary from rockyou and crackstation entries..."
    [ -e rockyou_resized.txt ] || get_rockyou
    [ -e crackstation-human-only_resized.txt ] || get_crackstation
    echo "Merging both dictionaries..."
    cat rockyou_resized crackstation-human-only_resized | sort -u > $DICT
fi

make 