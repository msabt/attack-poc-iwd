# Simulation environment

This directory contains all the material to test the PoC on virtual network interfaces. 

The simulation can be summarized into two scripts:

## Setup script

Running `setup.sh` will compile both iwd (with the ell library) and hostapd in order to emulate both an client (iwd) and an access point (hostapd).
All the sources are available in [build_aux](build_aux/) and will be installed in [local_install](local_install/). The script also compiles the spy process.

```
Usage: setup.sh -i|-u|-c
	-i: locally install iwd and hostapd
	-u: uninstall the local iwd and hostapd
	-c: clean build directories
```

Note that we added a few instructions in iwd source code (file sae.c) in order to easily access debug information. Namely, we print the current MAC addresses and the iteration at which the password is derived. This is only for debug purposes (evaluate the success rate or our attack), and does not impact the attack process.

## Trace measurement and attack

Running `poc.sh` allows to the following
* setup the environment (namely the virtual interfaces), 
* run iwd (daemon and client) and hostapd in those interfaces,
* establish some connections between the client and the AP, and measure the traces using the spy process,
* parse the traces to guess the number of round(s) needed to derive the password in a specific exchange,
* in debug mode, the real number of round is saved, allowing to evaluate the success rate of the attack.

```
Usage: ./poc.sh [-n NB_SAMPLES] [-d] -a PASSWD|-s PASSWD|-p TRACES_DIR
	-a: simulate the attack on PASSWD and parse the resulting trace to guess the result. Same as -sp.
	-s: simulate the attack on PASSWD (i.e.: collect traces).
	-p: parse the traces located in TRACES_DIR. This directory should contains a subdirectory for each address couple. Each subdirectory must contain two file: 
		* trace: contains the trace
		* debug: contains additionnal informations such as MAC addresses and real number of rounds in dubug mode (see -d).
	-d: run in debug mode. Display additional information (namely during the parsing).
	-n: perform te action with NB_SAMPLES samples. With -s, it collect NB_SAMPLES for each MAC address. With -p, it only uses the first NB_SAMPLES sample of each trace. The default value is 15.
```

The outupt traces will be stored in a directory named `res_traces`, with a subdirectory for each tested password.

For each password, 10 different addresses are tested.

### Example

For instance, the following command will acquire traces for the password `super_strong_password`, for 10 different MAC addresses, repeating the connections 14 times for each addresses:
```bash
sudo ./poc.sh -n 14 -s super_strong_password
```
The results are interpreted using the following command:
```bash
sudo ./poc.sh -n 14 -p res_traces/super_strong_password
```

We can combine these two commands with
```bash
sudo ./poc.sh -n 14 -a super_strong_password
```

## Disclamer

This scripts are part of a PoC and may lack stability. For instance, we add some issue with our AP detection or the client may crash once in a while. If that happe,s to you, simply re-launch the script.
Also note that addresses to monitor are hardcoded in `poc.sh`. Hence, building on a different system, or with different option (optimizations, ...) may yield different results.
