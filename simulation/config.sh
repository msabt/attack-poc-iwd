# On quitting, we stop all processes
trap terminate SIGINT

# Name of both AP and client interfaces
INTERFACE_AP="wlan1"
INTERFACE_CLI="wlan2"

SSID="WPA3-Network" 

# Some directories and files locations
SIMULATION_DIR=$(pwd)

BUILD_DIR="${SIMULATION_DIR}/build_aux"
IWD_BUILD_DIR="${BUILD_DIR}/iwd-1.5"
HOSTAPD_BUILD_DIR="${BUILD_DIR}/hostapd-2.9/hostapd"

LOCAL_INST_DIR="${SIMULATION_DIR}/local_install"
HOSTAPD_BIN="${LOCAL_INST_DIR}/bin/hostapd"

IWD_BIN="${LOCAL_INST_DIR}/libexec/iwd"
IWCTL_BIN="${LOCAL_INST_DIR}/bin/iwctl"
IWD_CACHE="${LOCAL_INST_DIR}/var/lib/iwd"

TMP_DIR="${SIMULATION_DIR}/tmp"
IWD_SOCK="${TMP_DIR}/iwctl.sock"
SPY_SOCK="${TMP_DIR}/spy.sock"

TRACE_DIR="${SIMULATION_DIR}/res_traces"

ROOT_DIR=$(cd .. && pwd && cd $SIMULATION_DIR)
SPY_BIN=${ROOT_DIR}/spy/spy
PARSER=${ROOT_DIR}/trace_interpretation/parsing.py


# Kill spawned process and remove temporary files
clean_up () {
	pkill sexpect
	pkill iwd
	pkill hostapd
    rm -rf ${TMP_DIR} ${IWD_CACHE}/*
	touch ${IWD_CACHE}/${SSID}.psk
}

# Remove folder with empty file; since they are synonym of errors
#	$1 is the directory containing the traces
remove_corrupted_traces() {
	# Remove empty and incomplete traces. A trace of 15 measurments 
	# should be at leat 8 Ko. We remove the whole directory if it 
	# contains a corrupted trace
	for path in $(find $1 -name trace -size -8k)
	do
		f=$(basename $path)
		rm -rf "${path/\/$f}"
	done
}

# Properly exit
terminate () {
	clean_up
	
	if [[ -d ${TRACE_DIR} ]];
	then
		chmod -R o+rw ${TRACE_DIR}
		remove_corrupted_traces ${TRACE_DIR}
	fi

	[[ -e ${SIMULATION_DIR}/ground_truth ]] && chmod o+rw ${SIMULATION_DIR}/ground_truth
	[[ -e ${SIMULATION_DIR}/prediction ]] && chmod o+rw ${SIMULATION_DIR}/prediction

	# Get our process group id
  	PGID=$(ps -o pgid= $$ | grep -o [0-9]*)
  	# Kill it in a new new process group
  	setsid kill -- -$PGID
}

# Setup virtual interfaces
setup_interfaces () {
	# Create new virtual interfaces and enable wifi on it
	modprobe mac80211_hwsim radio=2
	rfkill unblock wifi

	# To monitor traffic in wireshark 
	ifconfig hwsim0 up

	# Set the channel of the radios to the same freq
	iwconfig $INTERFACE_AP channel 1
	iwconfig $INTERFACE_CLI channel 1

	echo "In case of interferences, you may need to kill interfering programs You can run the following:"
	echo -e "\tairmoin-ng check kill"
	echo -e "\tpkill wpa_supplicant"
}
