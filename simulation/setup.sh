#!/bin/bash

set -e
source config.sh

install_iwd() {
    cd ${IWD_BUILD_DIR}
    ./bootstrap
    ./configure --prefix=${LOCAL_INST_DIR} --disable-systemd-service
    make -j
    echo "Need sudo to deploy d-bus config" 
    sudo make install
    cd ${SIMULATION_DIR}
}

clean_iwd() {
    cd ${IWD_BUILD_DIR}
    make clean
    cd ${SIMULATION_DIR}
}

install_hostapd() {
    cd ${HOSTAPD_BUILD_DIR}
    [ "$(grep -E "^CONFIG_SAE=y$" .config)" ] || echo "CONFIG_SAE=y" >> .config
    make -j 
    cp hostapd ${HOSTAPD_BIN}
    cd ${SIMULATION_DIR}
}

clean_hostapd() {
    cd ${HOSTAPD_BUILD_DIR}
    make clean
    cd ${SIMULATION_DIR}
}

build_spy() {
    cd ${ROOT_DIR}/spy
    [[ -f spy ]] || make
    cd ${SIMULATION_DIR}
}

uninstall() {
    rm -rf ${LOCAL_INST_DIR}/*
    echo "Need sudo to remove d-bus config" 
    sudo rm /usr/share/dbus-1/system.d/iwd-dbus.conf
}


while getopts "iuc" opt; do
    case "$opt" in
    i)
        install_iwd
        install_hostapd
        build_spy
        ;;
    u)  
        uninstall
        ;;
    c)  
        clean_iwd
        clean_hostapd
        ;;
    *)
        echo "Usage: $0 -i|-u|-c"
        echo -e "\t-i: locally install iwd and hostapd"
        echo -e "\t-u: uninstall the local iwd and hostapd"
        echo -e "\t-c: clean build directories"
    esac
done
