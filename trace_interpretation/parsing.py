from sys import argv, exit
import re

# The number of cycle we consider sufficient to consider the line as relevant
threshold = 350
# The threshold we consider sufficient to stop the trace interpretation
breaking_threshold = 1000

# Label used to distinguish the timer
label_timer = "kdf_sha256"
# Label used to distinguish the rigth iteration
label_found = "l_getrandom"

def parse_one_trace(t, res):
    n_rounds = 1
    finding = False
    start = False
    for line in t:
        # Lasts rounds tend to yield very high scores, but are really 
        # unlikely. It is worth ignoring them
        if n_rounds > 17:
            break

        # Skip blank lines to avoid errors
        if line == "":
            continue
        (event, pause, _) = line.split()
        pause = int(pause)

        # We can encounter some outliers at the end, it usually means 
        # we will only break our results if we keep going
        if pause > 8000:
            break

        if finding:
            if label_found in event:
                x = pause
                continue
            else:
                res[n_rounds] += x
                n_rounds += 1
                finding = False
                if x > breaking_threshold:
                    break
                continue
        elif pause > threshold:
            if label_timer in event:
                n_rounds += 1
            elif label_found in event:
                x = pause
                finding = True
                
    return res

# Parse each trace contained in traces and return the sums of
# the score for each possible iteration (1 to 20).
# Also return the number of processed traces.
def parse_traces(traces, n):
    trace = []
    res = [0 for i in range(21)]
    k = 0
    for line in traces:       
        # We found a separator, which means we are done reading the
        # current trace and we can process it
        if "=" in line:
            # Remove leading and trailing calls to getrandom (not relevant)
            while label_found in trace[-1]:
                trace = trace[:-1]
            while label_found in trace[0]:
                trace = trace[1:]
            # We start at index 1 since we know the first is a timer label
            res = parse_one_trace(trace[1:], res)
            # We only process n traces
            if k == n:
                break
            k += 1
            trace = []
        else:
            trace.append(line)
    return res, k

def check_debug_infos(filename):
    with open(filename) as fp:
        infos = fp.readlines()
    #We start at the third line since we experienced some issues with the first 3 during intensive simulations
    A = infos[3]
    B = infos[4]
    C = infos[5] 
    for i in range(6, len(infos), 3):
        if infos[i] == "":
            break
        if A != infos[i]:
            print("\t[WARNING] line", i+1, "macA changed:", A.rstrip(), " ->", infos[i].rstrip())
        if B != infos[i+1]:
            print("\t[WARNING] line", i+2, " macB changed:", B.rstrip(), " ->", infos[i+1].rstrip())
        if C != infos[i+2]:
            print("\t[WARNING] line", i+3, " changes in pwd:", C.rstrip(), " ->", infos[i+2].rstrip())
    
    return A.split()[1].rstrip(), B.split()[1].rstrip(), int(C.split()[1])


if len(argv) < 2:
    print("Usage: {} [-d] [-n NB_TRACES_TO_USE] TRACE_FOLDER [TRACE_FOLDER ...]".format(argv[0]))
    exit(-1)

if argv[1] == "-d" or argv[3] == "-d":
    debug = True
    args = argv[2:]
else:
    debug = False
    args = argv[1:]

n_traces = -1
if argv[1] == "-n":
    n_traces = int(argv[2])
    args = args[2:]
elif argv[2] == "-n":
    n_traces = int(argv[3])
    args = args[2:]

# Regular expression used to preprocess the trace, and remove false traces accidently recorded
pattern1 = re.compile('===========\n(^[^=\n]*$\n){,6}\n=', re.MULTILINE)

out = []
nb_error_without_warning=0
for trace_dir in args:
    if debug:
        print(trace_dir + ": ")
    warning = False
    # Recover debug infos and check them for sanity (no MAC address or password during the trace recording)
    A, B, nb_rounds = check_debug_infos(trace_dir + "/debug")

    # Pre-process the traces: remove false traces and split in lines
    with open(trace_dir + "/trace") as fp:
        traces = fp.read()
        traces = pattern1.sub('=', traces)
        traces = traces.split('\n')

    # res contains a score for each possible iteration, n is the number of traces processed
    res, k = parse_traces(traces, n_traces)
    if debug and n_traces > 0 and k != n_traces:
        print("\t[WARNING] You asked to process {} traces, but only {} were found".format(n_traces, k))
    total_score = sum(res)
    if total_score == 0:
        if debug:
            print("error during measurment")
        continue

    # Compute the frequency for each value fond in the traces
    freq = []
    while sum(res) != 0:
        m = max(res)
        i = res.index(m)
        res[i] = 0
        # x represents the probability of the value i being the right one
        x = m*100.0/total_score 
        if x < 5:
            break
        freq.append((i, round(x, 2)))

    if debug:
        print("\t*", A)
        print("\t*", B)
        print("\t* " + " - ".join(["{} ({}%)".format(x[0], x[1]) for x in freq])) 
    
    # Check for indicator of unreliable result
    two_likely_results =  (len(freq) > 1 and freq[0][1] - freq[1][1] <= 15)
    best_not_likely = freq[0][1] < 10
    if two_likely_results or best_not_likely :
        warning = True
        if debug:
            print("\tWARNING! Not quite sure of the result here...")   
    if nb_rounds != freq[0][0]:
        if debug:
            print("\t[DEBUG] Wrong result ! Expected", nb_rounds)
        if not warning:
            nb_error_without_warning += 1


    # Only keep the result if we are quite sure
    if not warning :
        if debug:
            # Debug files, used to evaluate the performance of our parsing
            with open("prediction_{}_traces".format(k), "a") as fp_pred:
                fp_pred.write(str(freq[0][0]) + "\n")
            with open("ground_truth_{}_traces".format(k), "a") as fp_truth:
                fp_truth.write(str(nb_rounds) + "\n")
        out.append((freq[0][0], A, B))


if debug:
    if nb_error_without_warning > 0:
        print("[DEBUG] {} errors without warnings".format(nb_error_without_warning))
for trace in sorted(out, reverse=True):
    print("{},{},{}".format(trace[1], trace[2], trace[0]), end=' ')
